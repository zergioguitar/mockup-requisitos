package MainPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;

/**
 *
 * @author sflores
 */
public class Colors
{
    /**
     * Color de los titulos que deben estar bien acentuados.
     */
    public static final Color titleColor = new Color(107,184,125);
    public static final Color titleColor2 = new Color(255, 102, 0);
    public static final Color titleColor3 = new Color(228, 204, 81);
    public static final Color titleColor4 = new Color(50,205,220);
    /**
     * Color de background panel izquierdo.
     */
    public static final Color panelBackgroundColor = Color.BLACK;
    
    /**
     * Color cuando el mouse pasa por encima del elemento.
     */
    public static final Color hoverColor = new Color(204,153,0);
    
    /**
     * Color de texto normal de la ventana principal.
     */
    public static final Color normalMainColor = Color.WHITE;
    
    /**
     * Color de texto normal del resto de las etiquetas.
     */
    public static final Color normalTextColor = Color.BLACK;
    
    /**
     * Color de texto al mostrar mensaje en el área de mensajeria.
     */
    public static final Color adviceColor = Color.RED;
}
