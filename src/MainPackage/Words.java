package MainPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sflores
 */
public class Words {
    public static final String ADD = "Agregar";
    public static final String EDIT = "Editar";
    public static final String DELETE = "Eliminar";
    public static final String PROFESOR = "Profesor";
    public static final String ADMINISTRADOR = "Administrador";
    public static final String MESSENGER = "Mensajería";
    public static final String LOGO_UTAL = "Img/icons/logo_utalca.jpg";

    public static String selectOneRow(String accion, String actor) {
        return "Debe seleccionar una fila antes de "+ accion + " un "+ actor;
    }
}
