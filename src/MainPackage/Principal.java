/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPackage;
import com.itextpdf.text.*;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.File;
import javax.swing.JTable;

/**
 *
 * @author gmardones
 */
public class Principal {
    
    private GenerarPDF pdf;
    private File ruta_destino;
    private Documento doc;
    
    
    float[] medidaCeldasOrdenDeCompra = {1.50f,2.00f,5.00f,2.00f,2.00f};     
    float[] medidaCeldasCotizaciones = {1.20f,1.25f,2.55f,1.55f};
    
    public Principal(Documento documento) 
    {
        pdf= new GenerarPDF();
        ruta_destino = null;
        this.doc =documento;
        creaDocumento();   
    }
    
       // create cells
    private static PdfPCell createLabelCell(String text){
        // font
        Font font = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.DARK_GRAY);
 
        // create cell
        PdfPCell cell = new PdfPCell(new Phrase(text,font));
        // set style
        Style.labelCellStyle(cell);
        return cell;
    }
 
    // create cells
    private static PdfPCell createValueCell(String text){
        // font
        Font font = new Font(FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
 
        // create cell
        PdfPCell cell = new PdfPCell(new Phrase(text,font));
        
        // set style
        Style.valueCellStyle(cell);
        return cell;
    }
    
    public void creaDocumento()
    {
         ruta_destino = pdf.Colocar_Destino(ruta_destino);
        //si destino es diferente de null
        if (this.ruta_destino != null) {
            try {

               PdfPTable tabla = new PdfPTable(this.doc.getColumnsName().length);
               tabla.setWidthPercentage(100f);
               
               Font font = new Font(FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.WHITE);
               
               PdfPCell celdaRelleno = new PdfPCell(new Paragraph(""));
               
               celdaRelleno.setRowspan(5);
               celdaRelleno.setBorderColorRight(BaseColor.WHITE);
               celdaRelleno.setBorder(com.itextpdf.text.Rectangle.BOTTOM);
               
               
               PdfPCell cell = new PdfPCell();
                celdaRelleno.setColspan(2);
                //tabla.setWidths(medidaCeldasCotizaciones);


                tabla.setWidths(new float[]{2,3,3,5,2});



                tabla.setWidthPercentage(100f);
                //table.setWidths(medidaCeldasCotizaciones);              
                // ----------------TITULO DE LA TABLA----------------
                cell = new PdfPCell(new Phrase("Detalle de Asistencia",font));
                cell.setColspan(6);
                // setear style
                Style.headerCellStyle(cell);
                tabla.addCell(cell);
               

                JTable tablaGeneral = new JTable(this.doc.getData(),this.doc.getColumnsName());

                for (int i = 0; i < tablaGeneral.getColumnCount(); i++)
                {
                    tabla.addCell(createLabelCell(" "+ this.doc.getColumnsName()[i]));
                }
                for (int f = 0; f < this.doc.getData().length; f++)
                {

                    for (int c = 0; c < tablaGeneral.getColumnCount(); c++)
                    {
                        tabla.addCell(createValueCell(" " + this.doc.getData()[f][c]));        
                                                    
                    }
                    
                }
                
                pdf.createCotizacion(this.doc, tabla,ruta_destino, PageSize.LETTER);

            } catch (Exception ex) {
                //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ruta_destino=null;
    }
    
}
