package Estadistica;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.Random;
import javax.swing.ImageIcon;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sflores
 */
public class PaneEstadistica extends javax.swing.JPanel {

    private BufferedImage image = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB); 
    private BufferedImage image2 = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB); 
    /**
     * Creates new form PanelProducto
     */
    public PaneEstadistica() {
        initComponents();
        drawChart();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelChart = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));

        panelChart.setSize(new java.awt.Dimension(440, 281));

        javax.swing.GroupLayout panelChartLayout = new javax.swing.GroupLayout(panelChart);
        panelChart.setLayout(panelChartLayout);
        panelChartLayout.setHorizontalGroup(
            panelChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 440, Short.MAX_VALUE)
        );
        panelChartLayout.setVerticalGroup(
            panelChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 281, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(panelChart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addComponent(panelChart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(90, 90, 90))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel panelChart;
    // End of variables declaration//GEN-END:variables

    private void drawChart() {
        JFreeChart barra = null;
        DefaultCategoryDataset datos;
        datos = new DefaultCategoryDataset();
        datos.setValue(Math.random()*50, "Asistencias", "");
        int fallas = (int) ((Math.random()*10)/3);
        datos.setValue(fallas, "Faltas", "");
        datos.setValue((Math.random()*10)/2, "Justificaciones", "");       
        barra = ChartFactory.createBarChart3D("Estadisticas Generales", "Semestre","Registros",datos,PlotOrientation.VERTICAL,true,true,true);
        BufferedImage graficoBarra=barra.createBufferedImage(panelChart.getWidth()*1, (int) (panelChart.getHeight()*1.7));
        this.image = graficoBarra;
        
        
        XYSeries series = null;
        XYDataset datos1;
        JFreeChart linea = null;
        series= new XYSeries("Ausencia a clases Anual");
        int x=1;
        series.add(fallas,x);
        x++;
        series.add(0.0001,x);
        x=12;
        series.add(0,x);
        
        datos1 = new XYSeriesCollection(series);
        linea = ChartFactory.createXYLineChart("Gráfico de Faltas","Ausencias","Meses",datos1,PlotOrientation.HORIZONTAL,true,true,true);
       
        BufferedImage graficoLinea=linea.createBufferedImage(panelChart.getWidth()*1, (int) (panelChart.getHeight()*1.7));
        
        this.image2 = graficoLinea;
    }

    @Override
    public void paint (Graphics g){
       //super.paintComponents(g);
       g.drawImage(this.image, panelChart.getX(), panelChart.getY(), null);
       g.drawImage(this.image2, panelChart.getX() + this.image.getWidth() + 20, panelChart.getY(), null);
    }
}
