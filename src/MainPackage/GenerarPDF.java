/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPackage;



import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GenerarPDF 
{        
    public PdfPTable formarTablaCotizacion (Documento doc, Document documento) throws DocumentException
    {
        
        Image foto = null;
        //AGREGAMOS IMAGEN
        try
        {
            foto = Image.getInstance(Words.LOGO_UTAL);
            foto.scaleToFit(200, 200);
            foto.setAlignment(Image.LEFT | Image.TEXTWRAP);
            //documento.add(foto);
        }
        catch ( Exception e )
        {
            System.out.println(""+e);
        }
  
        com.itextpdf.text.Font font = new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.HELVETICA, 12, com.itextpdf.text.Font.BOLD, BaseColor.WHITE);

        PdfPTable tablaOrden = new PdfPTable(2);
        tablaOrden.setWidthPercentage(100);


        BaseColor color = new BaseColor(195,194,220);
         
        PdfPCell orden;
        orden = new PdfPCell(new Paragraph("REPORTE"));
        orden.setBorder(Rectangle.TOP);
        orden.setUseBorderPadding(true);
        //orden.setBorderWidth(5f);
        orden.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        orden.setBorderColor(color);
        orden.setBackgroundColor(color);
        orden.setColspan(2);

        
        tablaOrden.addCell(orden);


        orden = new PdfPCell(new Paragraph("N° "+ 1 ));
        orden.setBorder(Rectangle.TOP);
        orden.setUseBorderPadding(true);
        //orden.setBorderWidth(5f);
        orden.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        
        orden.setBorderColor(color);
        orden.setBackgroundColor(color);
        orden.setColspan(2);

        tablaOrden.addCell(orden);

        
        orden = new PdfPCell(new Paragraph("Fecha: "+ doc.getFecha()));
        orden.setBorder(Rectangle.TOP);
        orden.setUseBorderPadding(true);
        //orden.setBorderWidth(5f);
        orden.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        orden.setBorderColor(color);
        orden.setBackgroundColor(color);
        orden.setColspan(2);

        tablaOrden.addCell(orden);
        
        


         //--------------GUARDAMOS EN LA TABLA 3 -----------------------
        PdfPTable table3 = new PdfPTable(3);
        table3.setWidthPercentage(100);
        table3.setWidths(new float[]{4f,0.6f,2.5f});

        PdfPCell cell2 = new PdfPCell(foto,true);
        cell2.setPaddingRight(2);
        cell2.setBorder(PdfPCell.NO_BORDER);    
        table3.addCell(cell2);

        PdfPCell  cell = new PdfPCell(new Phrase(" "));
        cell.setBorder(Rectangle.NO_BORDER);
        table3.addCell(cell);

        PdfPCell cell3 = new PdfPCell(tablaOrden);
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell.setBackgroundColor(BaseColor.WHITE);
        cell3.setPaddingLeft(2);
        table3.addCell(cell3);

         return table3;
    
    
    }
    
    public void createCotizacion(Documento doc, PdfPTable tabla2, File Destino, Rectangle TamanioPagina) throws DocumentException {
        /*Declaramos documento como un objeto Document
         Asignamos el tamaño de hoja y los margenes */
        Document documento = new Document(TamanioPagina, 80, 80, 75, 75);

        //writer es declarado como el método utilizado para escribir en el archivo
        PdfWriter writer = null;


        try {
            //Obtenemos la instancia del archivo a utilizar
            writer = PdfWriter.getInstance(documento, new FileOutputStream(Destino + ".pdf"));
        } catch (FileNotFoundException | DocumentException ex) {
            ex.getMessage();
        }

        //Agregamos un titulo al archivo
        documento.addTitle("Universidad de Talca");

        //Agregamos el autor del archivo
        documento.addAuthor("Marcela Pacheco V");

        //Abrimos el documento para edición
        documento.open();



        try 
        { 
            
            
            PdfPTable tablaCotizacion = formarTablaCotizacion(doc, documento);
            documento.add(new Paragraph(" "));
            documento.add(tablaCotizacion);
 
            documento.add(new Paragraph(" "));
            documento.add(new Paragraph(" "));
            documento.add(tabla2);
            documento.add(new Paragraph(" "));
            

        } catch (DocumentException ex) {
            ex.getMessage();
        }

        documento.close(); //Cerramos el documento

        writer.close(); //Cerramos writer

        try {
            File path;
            path = new File(Destino + ".pdf");
            Desktop.getDesktop().open(path);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public File Colocar_Destino(File ruta_destino) {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivo PDF", "pdf", "PDF");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(filter);
        int result = fileChooser.showSaveDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            ruta_destino = fileChooser.getSelectedFile().getAbsoluteFile();
        }
        return ruta_destino;
    }
}
