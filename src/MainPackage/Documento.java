/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPackage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author sflores
 */
class Documento {
    private String titulo;
    private String fecha;
    private DefaultTableModel table;
    private DateFormat dateFormat;
    private int numero;

    public Documento(DefaultTableModel model) {
        this.titulo = "Reporte General";
        this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.fecha = dateFormat.format(new Date());
        this.table = model;
        this.numero = 1+ (int) (Math.random()*10);
    }

    public String getTitulo() {
        return titulo;
    }

    public String getFecha() {
        return fecha;
    }

    public DefaultTableModel getTable() {
        return table;
    }

    public Object[] getColumnsName() {
        ArrayList<String> nombres = new ArrayList<>();
        for(int i=0; i<this.table.getColumnCount();i++)
        {
            nombres.add(this.table.getColumnName(i));
        }
        return nombres.toArray();
    }

    public int getNumero() {
        return numero;
    }
    
    public Object[][] getData (){
        Object[][] data = new Object[this.table.getRowCount()][this.table.getColumnCount()];
        for(int i=0; i<this.table.getRowCount();i++){
            for(int j=0; j<this.table.getColumnCount();j++){
                data[i][j] =this.table.getValueAt(i, j);
            }
        }
        return data;
    }
    
}
